from app.settings.base import *

DEBUG = True

MEDIA_ROOT = '/var/www/quiz/media'
STATIC_ROOT = '/var/www/quiz/static'