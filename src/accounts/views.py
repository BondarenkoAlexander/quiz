from django.contrib import messages
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, UpdateView

from accounts.forms import AccountRegistrationForm, AccountUpdateForm
from accounts.models import User


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User registered successfully')
        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.info(self.request, f'User {self.request.user} has been successfully logged in!')
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'
    success_url = reverse_lazy('index')

    def get_redirect_url(self):
        return reverse('index')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            messages.success(self.request, f'User {self.request.user} logged out')
        return super().dispatch(request, *args, **kwargs)


class AccountUpdateView(UpdateView):
    model = User
    template_name = 'profile.html'
    success_url = reverse_lazy('index')
    form_class = AccountUpdateForm

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f'Profile user {self.request.user} updated')
        return result


class AccountPasswordChangeView(PasswordChangeView):
    template_name = 'password-change.html'
    success_url = reverse_lazy('index')
