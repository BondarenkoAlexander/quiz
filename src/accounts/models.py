from PIL import Image
from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    rating = models.PositiveIntegerField(default=0)
    image = models.ImageField(default='default.png', upload_to='accounts')

    @staticmethod
    def get_rating():
        return User.objects.exclude(rating=0).order_by("-rating").only("username", "rating", "image")

    def get_results(self):
        results = []
        for temp_result in self.results.all().order_by('test'):
            results.append(temp_result.__str__())
        return results

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        image = Image.open(self.image)
        image.thumbnail((300, 300), Image.ANTIALIAS)
        image.save(self.image.path)
