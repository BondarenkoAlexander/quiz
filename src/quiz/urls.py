from django.urls import path

from quiz.views import TestDetailView, TestListView, TestResultCreateView, TestResultQuestionUpdateView, \
    TestResultDetailView, TestResultUpdateView, RatingDetailView, DeleteResultView

app_name = 'tests'

urlpatterns = [
    path('', TestListView.as_view(), name='list'),
    path('<uuid:uuid>/', TestDetailView.as_view(), name='details'),
    path('rating/', RatingDetailView.as_view(), name='rating'),
    path('<uuid:uuid>/results/create', TestResultCreateView.as_view(), name='result_create'),
    path('<uuid:uuid>/results/<uuid:result_uuid>/details', TestResultDetailView.as_view(), name='result_details'),
    path('<uuid:uuid>/results/<uuid:result_uuid>/delete', DeleteResultView.as_view(), name='result_delete'),
    path('<uuid:uuid>/results/<uuid:result_uuid>/update', TestResultUpdateView.as_view(), name='result_update'),
    path('<uuid:uuid>/results/<uuid:result_uuid>/questions/next',
         TestResultQuestionUpdateView.as_view(),
         name='question'),
]

