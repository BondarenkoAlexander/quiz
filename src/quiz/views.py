from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render  # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views.generic.list import MultipleObjectMixin

from accounts.models import User
from quiz.forms import ChoiceFormSet
from quiz.models import Test, Result, Question


class TestListView(LoginRequiredMixin, ListView):
    model = Test
    template_name = 'quiz/list.html'
    context_object_name = 'tests'


class TestDetailView(LoginRequiredMixin, DetailView, MultipleObjectMixin):
    model = Test
    template_name = 'quiz/details.html'
    context_object_name = 'test'
    pk_url_kwarg = 'uuid'
    paginate_by = 2

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.model.objects.get(uuid=uuid)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(object_list=self.get_queryset(), **kwargs)
        return context

    def get_queryset(self):
        return Result.objects.filter(
            test=self.get_object(),
            user=self.request.user
        )


class TestResultCreateView(LoginRequiredMixin, CreateView):

    def post(self, request, uuid):
        result = Result.objects.create(
            test=Test.objects.get(uuid=uuid),
            user=request.user,
            state=Result.STATE.NEW,
            current_order_number=0,
        )
        result.save()

        return HttpResponseRedirect(reverse(
            'tests:question',
            kwargs={
                'uuid': uuid,
                'result_uuid': result.uuid,
                # 'order_number': 1
            }
        ))


class TestResultDetailView(LoginRequiredMixin, DetailView):
    model = Result
    template_name = 'results/details.html'
    context_object_name = 'result'

    def get_object(self):
        uuid = self.kwargs.get('result_uuid')
        return self.get_queryset().get(uuid=uuid)


class TestResultQuestionUpdateView(LoginRequiredMixin, UpdateView):

    def get(self, request, uuid, result_uuid):

        order_number = Result.objects.get(uuid=result_uuid).current_order_number + 1

        question = Question.objects.get(
            test__uuid=uuid,
            order_number=order_number
        )

        choices = ChoiceFormSet(queryset=question.choices.all())

        return render(
            request=request,
            template_name='quiz/questions.html',
            context={
                'question': question,
                'choices': choices
            }
        )

    def post(self, request, uuid, result_uuid):

        order_number = Result.objects.get(uuid=result_uuid).current_order_number + 1

        question = Question.objects.get(
            test__uuid=uuid,
            order_number=order_number
        )

        choices = ChoiceFormSet(data=request.POST)
        selected_choices = [
            'is_selected' in form.changed_data
            for form in choices.forms
        ]

        result = Result.objects.get(
            uuid=result_uuid
        )

        if len(set(selected_choices)) == 1:
            messages.error(self.request, "All false or all true")
            return HttpResponseRedirect(reverse(
                'tests:question',
                kwargs={
                    'uuid': uuid,
                    'result_uuid': result.uuid,
                    # 'order_number': order_number+1
                }
            ))

        result.update_result(order_number, question, selected_choices)

        if result.state == Result.STATE.FINISHED:
            return HttpResponseRedirect(reverse(
                'tests:result_details',
                kwargs={
                    'uuid': uuid,
                    'result_uuid': result.uuid,
                }
            ))
        else:
            return HttpResponseRedirect(reverse(
                'tests:question',
                kwargs={
                    'uuid': uuid,
                    'result_uuid': result.uuid,
                    # 'order_number': order_number+1
                }
            ))


class TestResultUpdateView(LoginRequiredMixin, UpdateView):
    def get(self, request, uuid, result_uuid):
        return HttpResponseRedirect(reverse(
            'tests:question',
            kwargs={
                'uuid': uuid,
                'result_uuid': result_uuid,
                # 'order_number': result.current_order_number+1
            }
        ))


class RatingDetailView(LoginRequiredMixin, ListView):
    model = User
    template_name = 'results/rating.html'


class DeleteResultView(LoginRequiredMixin, DeleteView):
    model = Result
    template_name = 'results/delete.html'
    pk_url_kwarg = 'result_uuid'
    success_url = reverse_lazy('tests:list')

    def get_object(self):
        uuid = self.kwargs.get('result_uuid')
        return self.model.objects.get(uuid=uuid)


def error_404(request, *args, **argv):
    return render(request, '404.html', status=404)


def error_500(request, *args, **argv):
    return render(request, '500.html', status=500)


def error_400(request, *args, **argv):
    return render(request, '400.html', status=400)


def error_403(request, *args, **argv):
    return render(request, '403.html', status=403)
