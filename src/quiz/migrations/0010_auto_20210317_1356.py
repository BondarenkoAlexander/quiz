# Generated by Django 3.1.6 on 2021-03-17 11:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0009_auto_20210317_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='test',
            name='image',
            field=models.ImageField(default='default.png', upload_to='covers'),
        ),
    ]
